import Vue from 'vue'
import Router from 'vue-router'

import Restricted from './components/Restricted'

Vue.use(Router)

let routes = [
  {
    path: '/private-page',
    name: 'Restricted',
    component: Restricted
  }
]


export default new Router({routes})